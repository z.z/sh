## WHY ?

- ***does not affect currently installed systems***

- ***saves bandwidth by using compressed root file system for installation instead of image*** 

- ***installing multiple distros on same partition or volume makes efficient use of storage*** 

- ***installed distro OR Live image can be used to install any of the available distros on internal or external drive***

## REQUIREMENTS

> **BTRFS** device

> unencrypted **FAT32** device

> unencrypted **BIOS boot** device, for LEGACY support
 
## DEPENDENCIES

**`dracut cryptsetup lvm2 btrfs-progs tar xz gzip zstd gawk curl util-linux`**

## HOW TO USE ?

> install the dependencies mentioned above , then run the following commands

```sh
git clone https://gitlab.com/z.z/sh.git --branch grub
cd sh
su root -c ./i
```

## NOTE

> For installation on an encrypted device, the device must remain open during the installation

> **LUKS2 ?** wait till GRUB's full support OR [check this](https://gitlab.com/z.z/sh/-/tree/master)

> Install **os-prober** to boot from other Operating systems, example: **windows**

| To use a custom config for: | inside cloned directory place it as: |
|-----------------------------|--------------------------------------|
| linux-kernel                | `.config`                            |
| portage                     | `make.conf`                          |
| NIXOS                       | `configuration.nix`                  |

## QUICK DEMO

> name this installation. This name will be used for bootentry and subvolumes

	 > NAME          : penguin  # any name without special characters
    
> to install a **DISTRO** enter it's **ID**

	[ID]     [DISTRO]  |  [ID]     [DISTRO]
	-------------------+-------------------
	 a        ARCH     |   b        VOID
	 c        ARTIX    |   d        GENTOO
	 e        FUNTOO   |   f        KISS
	 g        ALPINE   |   h        NIXOS
	
	 > DISTRO ID     : c        # c is for ARTIX, choose any of the above

#### vital packages

> **ARCH**

	> packages  : cryptsetup grub efibootmgr dracut base linux lvm2 dhcpcd
     
> **VOID**

	> packages  : cryptsetup grub-x86_64-efi dracut base-system lvm2 dhcpcd

> **ARTIX**

	> packages  : cryptsetup grub efibootmgr dracut base linux lvm2 dhcpcd-<init> seatd-<init>

> **GENTOO**

	> packages  : cryptsetup grub efibootmgr gentoo-sources dhcpcd

> **FUNTOO**

	> packages  : cryptsetup grub efibootmgr dhcpcd

> **KISS**

	> packages  : cryptsetup grub efibootmgr baseinit dhcpcd


> **ALPINE**

	> packages  : cryptsetup grub-efi grub-bios linux-lts lvm2 dhcpcd

> **NIXOS**

	> packages  :

#### partition table for this demo

```sh
    NAME            FSTYPE
    sda             
    ├─sda1          vfat
    ├─sda2          
    ├─sda3          ntfs
    ├─sda4          vfat
    ├─sda5          btrfs
    ├─sda6          crypto_LUKS
    │ └─cryptroot   btrfs
    ├─sda7          crypto_LUKS
    │ └─cryptlvm    LVM2_member
    │   └─vg1-a     btrfs
    ├─sda8          LVM2_member
    │ ├─vg2-a       btrfs
    │ └─vg2-b      
    ├─sda9          LVM2_member
    │ ├─vg3-a       crypto_LUKS
    │ │ └─cryptlvm2 LVM2_member
    │ │   └─vg4-d   btrfs
    │ └─vg3-b      
┌┈▶ └─sda10         LVM2_member
└┬▶ sdb             LVM2_member
 └┈┈cryptraid       crypto_LUKS
    └─raidA         btrfs
┌┈▶ sdw             LVM2_member
├─▶ sdx             LVM2_member
├─▶ sdy             LVM2_member
└┬▶ sdz             LVM2_member
 └┈─raidB           btrfs
```

> select FAT device

```sh
	/dev/sda1
	/dev/sda4

> FAT device : /dev/sda4
```

```sh
	/dev/sda5
	/dev/mapper/cryptroot
	/dev/mapper/vg1-a
	/dev/mapper/vg2-a
	/dev/mapper/vg4-d
	/dev/mapper/raidA
	/dev/mapper/raidB
```

#### unencrypted file system

> **BTRFS partition**

	> ROOT device : /dev/sda5
 
> **LVM**

	> ROOT device : /dev/mapper/vg2-a
  
> **[LVM-RAID](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes)**

	> ROOT device : /dev/mapper/raidB
      
#### encrypted file system

> **[LUKS on partition](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_a_partition)**

	> ROOT device : /dev/mapper/cryptroot

> **[LVM on LUKS](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LVM_on_LUKS)**

	> ROOT device : /dev/mapper/vg1-a 

> **[LUKS on LVM](https://wiki.archlinux.org/title/Dm-crypt/Encrypting_an_entire_system#LUKS_on_LVM)**

	> ROOT device : /dev/mapper/vg4-d
 
> **LUKS on [LVM-RAID](https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/configuring_and_managing_logical_volumes/assembly_configure-mange-raid-configuring-and-managing-logical-volumes)**

	> ROOT device : /dev/mapper/raidA
    
> AFTER installation completes,
  access to the **shell** of installed distro will be granted.
  set password for root user and configure the installed distro

	passwd root

> REBOOT

## how to update GRUB ?

> if multiple distros are installed, append (update) entries with

	grub-mkconfig >> /fat/grub/grub.cfg

> unneeded entries can be carefully removed from /fat/grub/grub.cfg

> incase something goes wrong 

	cp /fat/grub.cfg /fat/grub/

<details>
<summary><b>How to Uninstall Distro? [Click to Expand]</b></summary>

> for example, lets uninstall GENTOO

```sh
mount /dev/.... /mnt    # mount ROOT device
rm -rf /mnt/GENTOO      # remove the folder
```

> now, remove unneeded entries from /fat/grub/grub.cfg

</details>

<details>
<summary><b>How to chroot? [Click to Expand]</b></summary>

```sh
OS=<name_provided_during_installation>
mount -o subvol="$OS" /dev/... /mnt   # mount ROOT device
mount /dev/... /mnt/fat               # mount FAT  device
mount -R /sys  /mnt/sys 
mount -R /dev  /mnt/dev 
mount -R /run  /mnt/run 
mount -R /proc /mnt/proc
chroot /mnt /bin/sh
/nix/var/nix/profiles/system/activate # for NIXOS only
```

</details>

<details>
<summary><b>create swap [Click to Expand]</b></summary>

> a swap file is always recommended

```sh
swapoff -a
rm -rf /.swap
btrfs subvolume create /.swap
truncate -s 0 /.swap/swap
chattr +C /.swap/swap
head -c 8G /dev/zero > /.swap/swap
chmod 600 /.swap/swap
mkswap /.swap/swap
printf "
/.swap/swap none swap defaults 0 0
" >> /etc/fstab
```

</details>

## Thanks 

**[ARCH](https://archlinux.org)
[VOID](https://voidlinux.org)
[ARTIX](https://artixlinux.org)
[GENTOO](https://www.gentoo.org)
[FUNTOO](https://www.funtoo.org)
[KISS](https://kisslinux.org)
[ALPINE](https://alpinelinux.org)
[NIXOS](https://nixos.org)**
