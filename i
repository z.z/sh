#!/bin/sh
# shellcheck disable=SC2059

set -e
export PATH="$PATH:/bin:/sbin:/usr/local/sbin:/usr/local/bin:/usr/bin:/usr/sbin:/run/wrappers/bin:/root/.nix-profile/bin:/etc/profiles/per-user/root/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin"

## FUNCTION

GO   () {

# block device info
BLK  () {
	lsblk -lspo name,type,fstype,uuid $1
	}
# get & extract
GET  () {
	ff="$TMP/ff"
	curl -L "$1" -o "$ff"
	tar -xpf "$ff" -C "$DIR/$2"
	rm "$ff"
	}
# filter & get
FET  () {
	FILTER=$(curl -L "$2" | awk -F '"' -v pat="$3.*[.](gz|xz|zst)\"" -v col="$1" -v cst="$4" -v st="$5" '$0 ~ cst {f=1}f && $0 ~ pat {printf substr($col,st); exit}')
	GET "$2$FILTER" "$6" || GET "$FILTER" "$6"
	# syntax :
	# FET <field separated by "> <URL> <REGEX to look for> <REGEX to look ahead> <index to print from> <directory to save in>
	}
# prepare volume
PRE  () {
	mkdir -p "$DIR"
	mount "$ROOT" "$DIR"
	btrfs subvolume create "$DIR/$OS"
	umount "$DIR"
	mount -o noatime,compress=zstd,space_cache=v2,subvol="$OS" "$ROOT" "$DIR"
	mkdir -p "$FMP" "$DIR/usr/src" "$TMP" "$DIR/etc/mkinitfs" "$DIR/etc/dracut.conf.d" "$DIR/run/shm"
	mount "$FAT" "$FMP"
	}
# chroot
EXE  () {
	# DNS
	printf 'nameserver 9.9.9.9' > "$DIR/etc/resolv.conf"
	# root user
	printf 'root::18678:0:99999:7:::' > "$DIR/etc/shadow"
	mount -R /sys  "$DIR/sys"
	mount -R /dev  "$DIR/dev"
	mount -R /run  "$DIR/run"
	mount -R /proc "$DIR/proc"
	# portage config
	cp make.conf "$DIR/etc/portage/"
	# kernel config
	cp .config "$TMP/"
	# initramfs modules
	printf 'dracutmodules+=" rootfs-block kernel-modules base crypt lvm "' > "$DIR/etc/dracut.conf.d/easy.conf"
	printf 'HOOKS=(base udev autodetect modconf block filesystems keyboard fsck keymap lvm2 encrypt)' > "$DIR/etc/mkinitcpio.conf"
	printf 'features="ata base ide scsi usb virtio btrfs lvm raid kms cryptsetup nvme"' > "$DIR/etc/mkinitfs/mkinitfs.conf"

	# cat my_setup >> "$ZET" && cp pacman.conf "$DIR/etc/"
	# execute chroot script
	printf "\n\n\n\n\n\n\n\n" | chroot "$DIR" sh /tmp/ZET
	# link init
	ln -s "$(ls "$DIR/bin/" | awk '/-init/ {printf $1; exit}')" "$DIR/bin/init"
	# kernel version
	KVER=$(ls "$DIR/lib/modules/" | awk '{printf $1; exit}')
	# generate initramfs
	dracut -N -f -m " rootfs-block kernel-modules base crypt lvm " "$DIR/boot/cust.img" --kver "$KVER" -k "$DIR/lib/modules/$KVER"
	# grub config
	GC="$DIR/etc/default/grub"
	printf "
GRUB_DEFAULT=0
GRUB_TIMEOUT=2
GRUB_GFXMODE=auto
GRUB_TERMINAL=console
GRUB_TIMEOUT_STYLE=menu
GRUB_DISTRIBUTOR=\"$OS\"
GRUB_ENABLE_CRYPTODISK=y
GRUB_GFXPAYLOAD_LINUX=keep
GRUB_DISABLE_RECOVERY=true
GRUB_DISABLE_OS_PROBER=false
GRUB_EARLY_INITRD_LINUX_CUSTOM=\"cust.img\"
GRUB_PRELOAD_MODULES=\"lvm luks luks2 part_gpt cryptodisk gcry_rijndael pbkdf2 gcry_sha256 btrfs\"
GRUB_CMDLINE_LINUX_DEFAULT=\"root=UUID=$RUID rd.luks.uuid=$LUID cryptdevice=UUID=$LUID:$OS cryptroot=UUID=$LUID cryptdm=$OS\"
" >  "$GC"
	# boot loader
	GIN="grub-install --boot-directory=. --efi-directory=. $DISK --target"
	printf "
cd /fat/
# legacy
$GIN=i386-pc
# efi
$GIN=x86_64-efi --removable
$GIN=x86_64-efi --bootloader-id=GRUB
grub-mkconfig >> grub/grub.cfg
# backup
cp grub/grub.cfg .
	" >> "$TMP/grub"
	chroot "$DIR" sh /tmp/grub

	# fstab
	printf "
UUID=$RUID  /  btrfs  rw,noatime,compress=zstd:3,space_cache=v2,subvol=/$OS  0 0
UUID=$FUID  /fat  vfat  rw,noatime,defaults  0 2
" > "$DIR/etc/fstab"
	}

## PROMPT

clear
# choose distro
printf "
 > NAME      : "
read -r OS

printf "
[ID]     [DISTRO]  |  [ID]     [DISTRO]
-------------------+-------------------
 a        ARCH     |   b        VOID
 c        ARTIX    |   d        GENTOO
 e        FUNTOO   |   f        KISS
 g        ALPINE   |   h        NIXOS

 > DISTRO ID : "
read -r DID

# list packages to install
printf "
 > packages  : "
read -r PKG

# select FAT device
BLK | awk '/vfat/ {printf "\n\t"$1"\n"}'
printf "
 > FAT  device : "
read -r FAT

# root device
BLK | awk '/btrfs/ {printf "\n\t"$1"\n"}'
printf "
 > ROOT device : "
read -r ROOT

## VARIABLES

# / directory
DIR="/tmp/$OS"
# temporary directory
TMP="$DIR/tmp"
# fat mount point
FMP="$DIR/fat"
# setup script
ZET="$TMP/ZET"
# memory
MEM="0$(awk '/MemAvailable/ {printf $2}' /proc/meminfo)"
# swap
SWP="0$(awk 'NR==2 {printf $3}' /proc/swaps)"
# available swap + memory
ASM=$(printf "$MEM $SWP" | awk '{printf substr((($1+$2)/2000000)+1,1,1)}')
# threads
CPU=$(nproc)
# number of jobs
JOB=$(printf "$ASM $CPU" | awk '{printf ($1>$2)*$2+($2>$1)*$1}')
# disk
DISK=$(BLK "$ROOT" | awk '/disk/ {printf $1; exit}')
# LUKS uuid
LUID=$(BLK "$ROOT" | awk '/LUKS/ {printf $4; exit}')
# root uuid
RUID=$(BLK "$ROOT" | awk 'NR==2 {printf $4}')
# fat uuid
FUID=$(BLK "$FAT" | awk 'NR==2 {printf $4}')
# mirror
MIR="https://mirrors.tuna.tsinghua.edu.cn"
# architecture
ARC="(x86_64|amd64)"
# install kernel
KIN="
cd /usr/src/linux-*/
cp /tmp/.config .
make -j $JOB
make INSTALL_MOD_STRIP=1 modules_install
make install
"

# installation begins
PRE
case "$DID" in
	a)
		FET 4 "$MIR/archlinux/iso/latest/" "$ARC"
		cp -r "$DIR/root.x86_64/." "$DIR"
		awk '{printf substr($0,2,length($0)) "\n"}' "$DIR/root.x86_64/etc/pacman.d/mirrorlist" > "$DIR/etc/pacman.d/mirrorlist"
		rm -r "$DIR/root.x86_64"
		printf "
pacman-key --init
pacman-key --populate archlinux
pacman -Syu $PKG
" >> "$ZET";;
	b)
		FET 4 "$MIR/voidlinux/live/current/" "$ARC-ROOTFS"
		printf "
rm -rf /var
xbps-install -R $MIR/voidlinux/current/ -S $PKG
xbps-remove base-voidstrap
xbps-remove -o
" >> "$ZET";;
	c)
		GET gitlab.com/z.z/tar/-/raw/master/pacman.tar
		printf "
pacman-key --init
pacman-key --populate artix
pacman -Syu $PKG
" >> "$ZET";;
	d)
		FET 4 "$MIR/gentoo/releases/amd64/autobuilds/current-install-amd64-minimal/" "stage3-$ARC-hardened-openrc"
		printf "
mkdir -p /etc/portage/repos.conf
cp /usr/share/portage/config/repos.conf /etc/portage/repos.conf/gentoo.conf

emerge-webrsync
env-update
source /etc/profile

USE=\"device-mapper\" emerge -uqDN --jobs=$JOB --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going $PKG

$KIN
" >> "$ZET";;
	e)
		GET build.funtoo.org/next/x86-64bit/generic_64/stage3-latest.tar.xz
		printf "
ego sync
USE=\"device-mapper\" emerge -uqDN --jobs=$JOB --exclude=rust --autounmask-continue=y --autounmask-license=y --keep-going $PKG

$KIN
" >> "$ZET";;
	f)
		FET 2 "github.com/kisslinux/repo/releases" "download\/" "" 25
		FET 2 "kernel.org" "linux" "longterm" "" "usr/src/"
		# kiss config
		printf "
export CFLAGS=\"-O3 -pipe -march=native\"
export CXXFLAGS=\"\$CFLAGS\"
export MAKEFLAGS=\"-j$JOB\"

export REPOS_DIR='/var/db/kiss'
export KISS_PATH=''
export KISS_SU=su

KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/core
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/extra
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/repo/wayland
KISS_PATH=\$KISS_PATH:\$REPOS_DIR/community/community
" >> "$DIR/etc/profile.d/kiss_path.sh"

		printf "
. /etc/profile.d/kiss_path.sh

cd \$REPOS_DIR/
git clone https://github.com/kisslinux/repo
git clone https://github.com/kiss-community/community

kiss b gnupg1
kiss i gnupg1

gpg --keyserver keys.gnupg.net --recv-key 13295DAC2CF13B5C
echo trusted-key 0x13295DAC2CF13B5C >> /root/.gnupg/gpg.conf

cd repo/
git config merge.verifySignatures true

kiss update
kiss update
kiss b libelf zstd perl $PKG
kiss i libelf zstd perl $PKG

$KIN
" >> "$ZET";;
	g)
		FET 4 "$MIR/alpine/latest-stable/releases/x86_64/" "minirootfs.*[^a-z].-$ARC"
		printf "
apk add $PKG
" >> "$ZET";;
	h)
		useradd nixbld
		useradd bld -G nixbld
		mkdir -m 755 /nix "$FMP/grub"
		curl -L https://nixos.org/nix/install | sh
		. "$HOME"/.nix-profile/etc/profile.d/nix.sh
		CHN=$(curl -L "$MIR/nixos-images" | awk -F '"' '/nix/{a=$6} END{print a}')
		nix-channel --add "$MIR/nix-channels/$CHN" nixpkgs
		nix-channel --update
		nix-env -f '<nixpkgs>' -iA nixos-install-tools
		nixos-generate-config --root "$DIR"
		printf "
{ config, pkgs, ... }:
{
 imports = [ ./hardware-configuration.nix ];
 boot = {
	loader.grub = {
		version = 2;
		enable = true;
		device = \"$DISK\";
		efiSupport = true;
		useOSProber = true;
		enableCryptodisk = true;
		efiInstallAsRemovable = true;
		extraEntries = ''
			menuentry \"OTHER\" {
			search --set=root --fs-uuid $FUID
			configfile /grub/grub.cfg}'';
	};
	loader.efi = {
		efiSysMountPoint = \"/fat\";
	};
 };
 environment.systemPackages = [ $PKG ];
}
" > "$DIR/etc/nixos/configuration.nix"
		cp configuration.nix "$DIR/etc/nixos/"
		printf "\n\n" | nixos-install --root "$DIR" &&
		printf "
menuentry \"$OS\" {
cryptomount -u $(printf "$LUID" | awk '{gsub("-","")}1')
search --set=root --fs-uuid $RUID
configfile /$OS/boot/grub/grub.cfg}
" >> "$FMP/grub/grub.cfg"
		ln -s /dev/null "$TMP/grub"
		printf "
/nix/var/nix/profiles/system/activate
" >> "$ZET";;
esac
EXE

printf '
run "passwd root" to set root password
Reboot, once done configuring
'
chroot "$DIR" sh
}

# check requirements
type dracut awk curl btrfs tar xz gzip zstd cryptsetup && pvs && lsblk && GO || printf '
ENTER   valid inputs
GRANT   root privileges
INSTALL dracut cryptsetup lvm2 btrfs-progs tar xz gzip zstd gawk curl util-linux
'
